export interface Message {
  message: string;
}

export interface ICompetitor {
    guid?: string
    kingside?: boolean
    contender?: boolean
    teamname: string
    color?: Color
    score: number 
    order: number
    streak?: number
    longestStreak?: number
}

export enum Color {
    Red = 'red',
    Blue = 'blue',
    Green = 'green',
    Purple = 'purple',
    Yellow = 'yellow',
    Pink = 'pink',
    Messi = 'messi'
}

export enum MessageType {
    Get = 'get',
    Kings = 'kings',
    Match = 'match',
    StartTimer = 'start-timer',
    StopTimer = 'stop-timer',
    SetTimer = 'set-timer',
    KingWin = 'king-win',
    ContenderWin = 'contender-win',
    MissedServe = 'missed-serve',
    AddPoint = 'add-point',
    RemovePoint = 'remove-point',
    MakeKing = 'make-king',
    MakeContender = 'make-contender',
    AddTeam = 'add-team',
    RemoveTeam = 'remove-team',
    MoveUp = 'move-up',
    MoveDown = 'move-down',
    ResetKings = 'reset-kings',

    MatchAddPoint = 'match-add-point',
    MatchRemovePoint = 'match-remove-point',
    MatchApplyTeams = 'match-apply-teams',
    MatchReset = 'match-reset',
    MatchEndSet = 'match-end-set',
    MatchSwitchSides = 'match-switch-sides',
}


export const createAction = (type: MessageType, payload?: any ) => {
    return JSON.stringify({
        type, 
        payload
    })
}


export interface IMatch {
    set: number
    home: MatchCompetitor
    away: MatchCompetitor
}

export interface MatchCompetitor{ 
    name: string
    color: Color
    sets: number[]
}

export enum HomeAway {
    Home = "home",
    Away = "away"
}

export interface IApplyTeamAction{
    home: {
        name: string
        color: Color
    }
    away: {
        name: string
        color: Color
    }
}

export const defaultMatchState = {
    set: 0,
    home: {
        name: 'Home',
        sets: [0],
        color: Color.Red,
    },
    away: {
        name: 'Away',
        sets: [0],
        color: Color.Blue,
    }
}
