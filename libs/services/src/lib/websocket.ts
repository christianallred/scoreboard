import {MessageType, createAction} from '@christian/api-interfaces'

// TODO: this probably isn't needed
const host = 'localhost:3000'

export let send
let onMessageCallback

export const startWebsocketConnection = () => {
    
    let connectionRetry = 0;
    let ws
    
    const connect = () => {
        ws = new window.WebSocket(`ws://${window.location.hostname}:3000`)

        ws.onopen = () => {
            console.log('opened ws connection')
        }

        ws.onclose = (e) => {
            console.log('close ws connection: ', e.code, e.reason)
            if (connectionRetry < 10){
                connectionRetry ++

                setTimeout(() => {  
                    console.log('retry websocket: ', connectionRetry)
                    connect()    
                }, 5000);
            }
        }

        ws.onmessage = (e) => {
            onMessageCallback && onMessageCallback(e.data)
        }
    }
    
    connect()

    send = ws.send.bind(ws)
}

// This function is called by our React application to register a callback
// that needs to be called everytime a new message is received
export const registerOnMessageCallback = (fn) => {
    console.log('registerOnMessageCallback')
    onMessageCallback = fn
}

export const handleMessage = (message, callback) => {
    const parsed = JSON.parse(message)

    switch(parsed.type){
        
    }
}



