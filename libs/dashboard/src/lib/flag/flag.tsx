import React from 'react'
import './flag.scss'

import { Color } from '@christian/api-interfaces'

interface IFlagProps {
    color: Color
    score?: number
    noAngle?: boolean
    small?: boolean
    micro?: boolean
    left?: boolean
    white?: boolean
}

export const Flag = ({score, color, noAngle, small, micro, left, white}: IFlagProps) => {
    const className = `flag ${color} ${noAngle ? 'no-clip': ''} ${left ? 'left' : ''}`

    const scoreClass = `flag-score ${small ? 'small' : ''} ${micro ? 'micro' : ''}  ${white ? 'white' : ''}`

    return <div className={className}>
        <span className={scoreClass}>{score}</span>
    </div>
}