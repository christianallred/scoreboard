import React from 'react'
import './nextup.scss'
import { Nextupline } from '../nextupline/nextupline'
import { ICompetitor } from '@christian/api-interfaces';

interface INextLineProps{
    competitors: ICompetitor[],
    hideHeader?: boolean
}

export const Nextup = ({competitors, hideHeader}: INextLineProps) => {
    const nextLines = competitors.map((comp, index) => <Nextupline {...comp} key={index} />)
    
    const stylestring = `next-up ${hideHeader ? 'hide-header': ''}`
    
    return (
        <div className={stylestring}>
            {!hideHeader ? <div className="next-up-header">
                <span>Next Up</span>
            </div> : null}
            <div className="rows">
                {nextLines}
            </div>  
        </div>
    )
    
}