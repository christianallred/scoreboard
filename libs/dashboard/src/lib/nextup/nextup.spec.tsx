import React from 'react';
import { render } from '@testing-library/react';

import Nextup from './nextup';

describe('Nextup', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Nextup />);
    expect(baseElement).toBeTruthy();
  });
});
