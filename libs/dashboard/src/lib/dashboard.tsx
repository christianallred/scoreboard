import React from 'react';

import './dashboard.scss';

/* eslint-disable-next-line */
export interface DashboardProps {}

export const Dashboard = (props: DashboardProps) => {
  return (
    <div>
      <h1>Welcome to dashboard!</h1>
    </div>
  );
};

export default Dashboard;
