import React from 'react';

import {MatchCompetitor} from '@christian/api-interfaces'

import './matchstreamteam.scss';

import {Flag} from '../flag/flag'

/* eslint-disable-next-line */
export interface MatchstreamteamProps extends MatchCompetitor{
}

export const Matchstreamteam = ({name, color, sets}: MatchstreamteamProps) => {

  const _sets = sets.map((set,index) => <div className="score" key={index}>{set}</div>)

  return (
    <div className='match-stream-team'>
      <Flag color={color} noAngle />
      <div className="name">{ name }</div>
      {_sets}
    </div>
  );
};

export default Matchstreamteam;
