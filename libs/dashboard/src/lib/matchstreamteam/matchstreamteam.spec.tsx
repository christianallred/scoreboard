import React from 'react';
import { render } from '@testing-library/react';

import Matchstreamteam from './matchstreamteam';

describe('Matchstreamteam', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Matchstreamteam />);
    expect(baseElement).toBeTruthy();
  });
});
