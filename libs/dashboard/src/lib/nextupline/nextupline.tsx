import React from 'react'
import './nextupline.scss'
import {Flag} from '../flag/flag'
import {ICompetitor} from '@christian/api-interfaces';

interface INextUpLineProps extends ICompetitor {
    key: number
}

export const Nextupline = ({ teamname, color, score}: INextUpLineProps) => {
    return (
        <div className="next-up-row">
            <span className="next-up-row-score">{score}</span>
            <Flag color={color}/>
            <span className="next-up-row-name">{teamname}</span>
        </div>
    )
}