import React from 'react';
import { render } from '@testing-library/react';

import Nextupline from './nextupline';

describe('Nextupline', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Nextupline />);
    expect(baseElement).toBeTruthy();
  });
});
