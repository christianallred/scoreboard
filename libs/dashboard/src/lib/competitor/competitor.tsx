import React from 'react'
import './competitor.scss'
import {Flag} from '../flag/flag'
import {ICompetitor, Color} from '@christian/api-interfaces';

import crown from './crown.png';

interface ICompetitorProps extends ICompetitor {
    hideHeader?: boolean
}

export const Competitor = ( {kingside, teamname, score, color, hideHeader} : ICompetitorProps) => {
    const stylestring = `competitor ${hideHeader ? 'hide-header': ''}`
    return (
        <div className={stylestring}>
            {!hideHeader ? 
                <div className="competitor-kingside-header">
                    <span>{ kingside ? 'Kingside' : 'Challenger'}</span> 
                </div> 
            : null}
            <div className="competitor-kingside-name">
                <span>{teamname}</span>
            </div>
            
            <Flag color={color} score={score} />
        </div>
    )
}