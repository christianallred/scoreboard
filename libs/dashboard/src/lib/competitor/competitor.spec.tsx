import React from 'react';
import { render } from '@testing-library/react';

import Competitor from './competitor';

describe('Competitor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Competitor />);
    expect(baseElement).toBeTruthy();
  });
});
