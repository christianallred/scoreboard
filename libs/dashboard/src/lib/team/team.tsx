import React from 'react'
import './team.scss'
import {ICompetitor} from '@christian/api-interfaces'
import {Flag} from '../flag/flag'
import {send} from '@christian/services'

import { 
  FaCrown,
  FaFighterJet,
  FaPlusCircle, 
  FaMinusCircle, 
  FaTimesCircle,
  FaArrowAltCircleUp, 
  FaArrowAltCircleDown 
} from "react-icons/fa";

import {
    MessageType,
    createAction,
} from '@christian/api-interfaces'


export const Team = ({guid, order, teamname, score, kingside, contender, color, longestStreak}: ICompetitor) => {
    return (
        <div className="control-team-list">
            <span>
                {kingside ? 'King' : ''}
                {contender ? 'Cont' : ''}
                {order}
            </span>
            <Flag noAngle color={color} score={`${score} (${longestStreak})`} micro />
            <span>{teamname}</span>
            <FaCrown onClick={() => send(createAction(MessageType.MakeKing, guid))} />
            <FaFighterJet onClick={() => send(createAction(MessageType.MakeContender, guid))} />
            <FaPlusCircle onClick={() => send(createAction(MessageType.AddPoint, guid))} />
            <FaMinusCircle onClick={() => send(createAction(MessageType.RemovePoint, guid))} />
            <FaTimesCircle onClick={() => send(createAction(MessageType.RemoveTeam, guid))} /> 
            {order > 1 ? <FaArrowAltCircleUp  onClick={() => send(createAction(MessageType.MoveUp, guid))} /> : null}
            {order > 1 ? <FaArrowAltCircleDown onClick={() => send(createAction(MessageType.MoveDown, guid))} />: null}
        </div>
    )
}