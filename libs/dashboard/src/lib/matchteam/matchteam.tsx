import React from 'react';

import './matchteam.scss';

import {MatchCompetitor} from '@christian/api-interfaces'

import {Flag} from '../flag/flag'

/* eslint-disable-next-line */
export interface MatchteamProps extends MatchCompetitor{
}

export const Matchteam = ({name, color, sets}: MatchteamProps) => {
  return (
    <div className='match-team'>
      <Flag color={color} />
      <div>{ name }</div>
      <div>{ sets[0] }</div>
    </div>
  );
};

export default Matchteam;
