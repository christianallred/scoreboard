import React from 'react';
import { render } from '@testing-library/react';

import Matchteam from './matchteam';

describe('Matchteam', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Matchteam />);
    expect(baseElement).toBeTruthy();
  });
});
