import React from 'react'
import './timer.scss'
import leftPad from 'left-pad'

interface ITimerProps{
    timeLeft: number,
    small?: boolean
}

export const Timer = ({timeLeft, small}: ITimerProps) => {

    var minutes = leftPad(Math.floor(timeLeft / 60), 2, 0);
    
    var seconds = leftPad(timeLeft % 60, 2, 0);

    const classstring = `timer ${small ? 'small' : ''}`
    
    return (
        <span className={classstring}>
            {minutes}:{seconds}
        </span>
    )
}