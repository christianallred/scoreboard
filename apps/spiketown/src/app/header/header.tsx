import React, { useEffect, useState } from 'react';
import './header.scss'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export const Header = () => {
    return (
        <div className="header">
            <Link to="/">Home</Link>
            <Link to="/kings/controls">Kings</Link>
            <Link to="/match/controls">Match</Link>
        </div>
    )
}
