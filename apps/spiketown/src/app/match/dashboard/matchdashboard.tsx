import React, {useState, useEffect} from "react";
import './matchdashboard.scss'
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import { Matchteam } from '@christian/dashboard'
import { 
    Color,
    MessageType, 
    defaultMatchState
} from '@christian/api-interfaces';
import {registerOnMessageCallback} from '@christian/services'


export const MatchDashboard = () => {
    // TODO: get this 
    const [state, setState] = useState(defaultMatchState)

    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Match){
            setState(parsed.payload)
        }
    }

    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, []) 
    
    const handle = useFullScreenHandle();

    return (
        <div className="match-dashboard">
            <button onClick={handle.enter} className="fullscreen-button">
                Enter fullscreen
            </button>
                
            <FullScreen handle={handle}>
                <Matchteam {...state.home} />
                <Matchteam {...state.away} />
            </FullScreen>
        </div>
    )
}