import React, {useState, useEffect} from "react";
import './matchstreamdashboard.scss'
import {
    Matchstreamteam
} from '@christian/dashboard'
import {
    Color, 
    ICompetitor, 
    MessageType, 
    defaultMatchState
} from '@christian/api-interfaces';
import {registerOnMessageCallback} from '@christian/services'

export const MatchStreamDashboard = () => {
    // TODO: get this 
    const [state, setState] = useState(defaultMatchState)

    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Match){
            setState(parsed.payload)
        }
    }

    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, []) 
    return (
        <div className="match-stream-dashboard">                
            <Matchstreamteam {...state.home} />
            <Matchstreamteam {...state.away} />
        </div>
    )
}