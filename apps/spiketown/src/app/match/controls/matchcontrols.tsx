import React , {useState, useEffect} from "react";
import './matchcontrols.scss'
import {
    startWebsocketConnection, 
    registerOnMessageCallback, 
    send
} from '@christian/services'
import {
    Color,
    MessageType,
    createAction,
    HomeAway,
    defaultMatchState,
} from '@christian/api-interfaces'
import{
    Flag
} from '@christian/dashboard'
import { 
  FaPlusCircle, 
  FaMinusCircle,
} from 'react-icons/fa'
import {
    IconContext
} from 'react-icons'


import {compact} from 'lodash'

import {
    ButtonGroup,
    Button,
    TextField,
    MenuItem,
    Select,
} from '@material-ui/core'

import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'

export const MatchControls = () => {
    //TODO:move the default state to somewhere else
    const [state, setState] = useState(defaultMatchState)

    const [homeName, setHomeName] = useState('')
    const [homeColor, setHomeColor] = useState(Color.Red as string)

    const [awayName, setAwayName] = useState('')
    const [awayColor, setAwayColor] = useState(Color.Blue as string)
    
    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Match){
            setState(parsed.payload) 
            if (parsed.payload.home && parsed.payload.home.name && parsed.payload.home.name !== homeName){
                setHomeName(parsed.payload.home.name)
            }
            if (parsed.payload.away && parsed.payload.away.name && parsed.payload.away.name !== awayName){
                setAwayName(parsed.payload.away.name)
            }
        }
    }

    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, []) 

    const colorList = []
    for (let _color in Color) {
        colorList.push(<MenuItem key={_color} value={Color[_color]}>{_color}</MenuItem>)
    }
    let homeSetString = ''
    if (state.home && state.home.sets && state.home.sets.length > 1){
        homeSetString = state.home.sets.slice(0, length - 1).map(value => value).join(', ')
    }

    let awaySetString = '';
    if (state.away && state.away.sets && state.away.sets.length > 1){
        awaySetString = state.away.sets.slice(0, length - 1).map(value => value).join(', ')
    }
    
    const applyChanges = () => {
        send(createAction(MessageType.MatchApplyTeams, {
            home: {
                name: homeName,
                color: homeColor,
            },
            away: {
                name: awayName, 
                color: awayColor,
            }
        }))
    }

    const resetMatch = () => {
        send(createAction(MessageType.MatchReset))
    }

    const endSet = () => {
        send(createAction(MessageType.MatchEndSet))
    }

    const switchSides = () => {
        send(createAction(MessageType.MatchSwitchSides))
    }

    return (
        <div className="match-controls">

            <div className="match-controls-header-wrapper">
                <IconContext.Provider value={{ size: "2em" }}>
                    <div className="match-controls-competitor">
                        <h3 className="match-controls-competitor-name">{state.home.name}</h3>
                        <div className="match-controls-competitor-set">
                            <small>{homeSetString}</small>
                            <span>{state.home.sets[state.set]}</span>
                        </div>
                        
                        <div className="match-controls-competitor-controls">
                            <ButtonGroup>
                                <Button variant="contained" color="primary" endIcon={<AddIcon />}
                                    onClick={() => send(createAction(MessageType.MatchAddPoint, HomeAway.Home))} >
                                    Add
                                </Button>
                                <Button variant="contained" color="secondary" startIcon={<RemoveIcon />}
                                    onClick={() => send(createAction(MessageType.MatchRemovePoint, HomeAway.Home))} >
                                    Subtract
                                </Button>
                            </ButtonGroup>
                        </div>
                        <div className="match-controls-competitor-controls">
                            <TextField label="Team Name" variant="outlined" value={homeName} 
                                onChange={ (e) => setHomeName(e.target.value)} />
                            <Select label="Color" variant="outlined" value={homeColor} onChange={ (e) => setHomeColor(e.target.value as string) }>
                                {colorList}
                            </Select>
                        </div>
                    </div>
                    <div className="match-controls-competitor">
                        <h3 className="match-controls-competitor-name">{state.away.name}</h3>
                        <div className="match-controls-competitor-set">
                            <small>{awaySetString}</small>
                            <span>{state.away.sets[state.set]}</span>
                        </div>
                        <div className="match-controls-competitor-controls">
                            <ButtonGroup>
                                <Button variant="contained" color="primary" endIcon={<AddIcon />}
                                    onClick={() => send(createAction(MessageType.MatchAddPoint, HomeAway.Away))} >
                                    Add
                                </Button>
                                <Button variant="contained" color="secondary" startIcon={<RemoveIcon />}
                                    onClick={() => send(createAction(MessageType.MatchRemovePoint, HomeAway.Away))}>
                                    Subtract
                                </Button>
                            </ButtonGroup>
                        </div>
                        <div className="match-controls-competitor-controls">
                            <TextField label="Team Name" variant="outlined" value={awayName} onChange={ (e) => setAwayName(e.target.value)} />
                            <Select label="Color" variant="outlined" value={awayColor} onChange={ (e) => setAwayColor(e.target.value as string) } >
                                {colorList}
                            </Select>
                        </div>
                    </div>
                </IconContext.Provider>
            </div>

            <div className="match-controls-footer">  
                <Button variant="contained" onClick={applyChanges}>Apply Changes</Button>
                <Button variant="contained" onClick={switchSides}>Switch Sides</Button>
                <Button variant="contained" onClick={endSet}>End Set</Button>
                <Button variant="contained" color="secondary" onClick={resetMatch}>Reset Match</Button>
            </div>
        </div>        
    )
}
    
