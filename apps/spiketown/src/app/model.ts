export enum Color {
    Red = 'red',
    Blue = 'blue',
    Green = 'green',
    Purple = 'purple',
    Yellow = 'yellow',
    Pink = 'pink',
}