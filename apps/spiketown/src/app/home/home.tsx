import React from 'react';

import './home.scss';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

/* eslint-disable-next-line */
export interface HomeProps {}

export const Home = (props: HomeProps) => {
  return (
    <div>
      <h1>Welcome to home!</h1>
      <ul>
      <li><Link to="/kings/controls">Kings Controls</Link></li>
      <li><Link to="/kings/dashboard">Kings Scoreboard</Link></li>
      <li><Link to="/kings/stream/timer">Kings stream timer</Link></li>
      <li><Link to="/kings/stream/teams">Kings stream teams</Link></li>

      <li><Link to="/match/controls">Match Controls</Link></li>
      <li><Link to="/match/dashboard">Match Scoreboard</Link></li>

      <li><Link to="/match/stream/dashboard">Match Stream Scoreboard</Link></li>
      </ul>
    </div>
  );
};

export default Home;

