import React , {useState, useEffect} from "react";
import './controls.scss'
import {
    startWebsocketConnection, 
    registerOnMessageCallback, 
    send
} from '@christian/services'
import {
    Color,
    MessageType,
    createAction,
} from '@christian/api-interfaces'
import {
    Button,
    TextField,
    MenuItem,
    FormControl,
    Select,
    ButtonGroup,
} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add';

import {compact} from 'lodash'
import {Timer, Flag, Team} from '@christian/dashboard'

export const Controls = () => {
    const [state, setState] = useState({
        teams: [],
        timer: 0
    })
    const [time, setTime] = useState('0')
    const [name, setName] = useState('')    
    const [color, setColor] = useState(Color.Red as string)

    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Kings){
            setState(parsed.payload)
        }
    }
    
    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, [])
    
    const king = state.teams.find(team => team.kingside)
    
    const contender = state.teams.find(team => team.contender)

    const teamList = compact([
        king,
        contender,
        ...state.teams.filter(t => !t.kingside && !t.contender)
    ]).sort((a,b) => a.order < b.order ? -1 : 1)
    .map( (team, index) => <Team {...team} key={index} />)
    
    const colorList = []
    for (let _color in Color) {
        colorList.push( <MenuItem key={_color} value={Color[_color]}>{_color}</MenuItem> )
    }  


    return (
        <div className="controls">
            <div className="controls-upper">
                <div className="controls-timer-wrapper">
                    <Timer timeLeft={state.timer} small/>
                    <div className="controls-timer-controls">
                        <ButtonGroup fullWidth>
                            <Button variant="outlined" color="primary" onClick={() => send(createAction(MessageType.StartTimer))} >
                                Start
                            </Button>
                            <Button variant="outlined" color="secondary" onClick={() => send(createAction(MessageType.StopTimer))} >
                                Stop
                            </Button>
                        </ButtonGroup>
                        <TextField size="small" label="Set Time" variant="outlined" value={time} onChange={ (e) => setTime(e.target.value)} />
                        <Button variant="outlined" onClick={() => send(createAction(MessageType.SetTimer, time))} >
                            Set Timer
                        </Button>
                    </div>
                </div>

                <div className="controls-contenders-section"> 
                    <section className="controls-contenders-upper">
                        <div className="controls-contenders-competitor">
                            <h4>Kingside</h4>
                            {king ? 
                                <div className="controls-contenders-contender">
                                    <h2>{ king.teamname }</h2>
                                    <Flag color={king.color} score={king.score} small noAngle/>
                                    <Button variant="outlined" color="primary" onClick={ () => send(createAction(MessageType.KingWin, king)) }>
                                        Won Point
                                    </Button>
                                </div>
                            : null }
                        </div>
                        <div className="controls-contenders-competitor">
                            <h4>Contender</h4>
                            {contender ? 
                                <div className="controls-contenders-contender">
                                    <h2>{contender.teamname}</h2>
                                    <Flag color={contender.color} score={contender.score} small noAngle/>
                                    <ButtonGroup fullWidth>
                                        <Button variant="outlined" color="primary" onClick={ () => send(createAction(MessageType.ContenderWin, contender))} >
                                            Won Point
                                        </Button>
                                        <Button variant="outlined" color="secondary" onClick={ () => send(createAction(MessageType.MissedServe, true))}>
                                            Missed Serve
                                        </Button>
                                    </ButtonGroup>
                                </div>
                            : null }
                        </div>
                    </section>

                    <div className="controls-team-list">
                        {teamList}
                        <div className="controls-add-team">
                            <Select variant="outlined" value={color} onChange={ (e) => setColor(e.target.value as string) } >
                                {colorList}
                            </Select>
                            <TextField label="Name" variant="outlined" value={name} onChange={ (e) => setName(e.target.value)} />
                            <Button size="small" variant="outlined" onClick={() => send(createAction(MessageType.AddTeam, {name, color}))}>
                                Add Team
                            </Button>                            
                        </div>
                    </div>                
                </div>
            </div>
            <div className="congrols-lower">
                <div className="controls-reset">
                
                <Button variant="outlined" color="secondary" onClick={() => send(createAction(MessageType.ResetKings))}>
                    Reset
                </Button>                            
        
                </div>
            </div>
        </div>        
    )
}
    
