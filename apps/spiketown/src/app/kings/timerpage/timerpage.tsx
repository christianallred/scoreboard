import React, { useState, useEffect } from "react";
import './timerpage.scss'
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import { Timer } from '@christian/dashboard'
import { MessageType } from '@christian/api-interfaces';
import { registerOnMessageCallback } from '@christian/services'

export const TimerPage = () => {
    const [state, setState] = useState({
        teams: [], 
        timer: 0
    })

    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Kings){
            setState(parsed.payload)
        }
    }

    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, [])
     
    const handle = useFullScreenHandle();

    return (
        <div className="timerpage">
            <FullScreen handle={handle}>
               <div className="timer-wrapper">
                    <Timer timeLeft={state.timer} />
                </div>
            </FullScreen>
        </div>
    )
}