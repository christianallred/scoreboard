import React, {useState,useEffect} from "react";
import './teamspage.scss'
import { FullScreen, useFullScreenHandle } from "react-full-screen";

import {Competitor, Nextup} from '@christian/dashboard'
import {Color, ICompetitor, MessageType} from '@christian/api-interfaces';
import {registerOnMessageCallback} from '@christian/services'


export const TeamsPage = () => {
    const [state, setState] = useState({
        teams: [], 
        timer: 0
    })

    const handleMessage = (msg: string) => {
        const parsed = JSON.parse(msg)
        if (parsed.type === MessageType.Kings){
            setState(parsed.payload)
        }
    }

    useEffect(() => {
        registerOnMessageCallback(handleMessage) 
    }, [])
     
    const handle = useFullScreenHandle();

    const king = state.teams.find(team => team.kingside)
    const contender = state.teams.find(team => team.contender)
    const nextUp = state.teams.filter(team => !team.kingside && !team.contender)
        .sort((a,b) => a.order < b.order ? -1 : 0 )

    return (
        <div className="teamspage">
            <FullScreen handle={handle}>
                <div className="competitors-wrapper">
                    <Competitor {...king} hideHeader/>
                    <Competitor {...contender} hideHeader/>
                    <Nextup competitors={nextUp} hideHeader/>
                </div>
            </FullScreen>
        </div>
    )
}