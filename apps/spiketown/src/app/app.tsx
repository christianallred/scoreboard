import React, { useEffect, useState } from 'react';
import './app.scss'
import { Message } from '@christian/api-interfaces';

import { Home } from './home/home'
import { Header } from './header/header'

import { Controls } from './kings/controls/controls'
import { Dashboard } from './kings/dashboard/dashboard'
import { TimerPage } from './kings/timerpage/timerpage'
import { TeamsPage } from './kings/teamspage/teamspage'
import {MatchControls} from './match/controls/matchcontrols'
import {MatchDashboard} from './match/dashboard/matchdashboard'
import {MatchStreamDashboard} from './match/streamdashboard/matchstreamdashboard'

import {startWebsocketConnection} from '@christian/services'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


export const App = () => {
  const [m, setMessage] = useState<Message>({ message: '' });
  
  // TODO: handle websocket disconnect
  startWebsocketConnection()
  
  return ( 
    <>
     <Router>
      <div className="app">
        <Switch>
          <Route exact path={["/","/kings/controls","/match/controls"]}>
            <Header />
          </Route>
        </Switch>

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/kings/controls">
            <Controls />
          </Route>
          <Route path="/kings/dashboard">
            <Dashboard />
          </Route>
          <Route path="/kings/stream/timer">
            <TimerPage />
          </Route>
          <Route path="/kings/stream/teams">
            <TeamsPage />
          </Route>

          <Route path="/match/controls">
            <MatchControls />
          </Route>
          <Route path="/match/dashboard">
            <MatchDashboard />
          </Route>
          <Route path="/match/stream/dashboard">
            <MatchStreamDashboard />
          </Route>


        </Switch>
      </div>
    </Router>
    </>
  );
};

export default App;
