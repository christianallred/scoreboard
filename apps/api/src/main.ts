import * as express from 'express';
import * as expressWs from 'express-ws'

import { wsHandler } from './ws'
import { Message } from '@christian/api-interfaces';
import * as path from 'path'

const baseApp = express();

const wsInstance = expressWs(baseApp)

let { app } = wsInstance

app.ws('/', wsHandler)

const greeting: Message = { message: 'Welcome to api!' };

app.get('/api', (req, res) => {
  res.send(greeting);
});


app.use(express.static(path.join(__dirname, "..", "spiketown")));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "spiketown", "index.html"));
});

const port = process.env.port || 3000;

const server = app.listen(port, () => {
  console.log('Client at http://localhost:' + port);
  console.log('API at http://localhost:' + port + '/api');
});

server.on('error', console.error);
