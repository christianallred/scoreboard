import { Kings } from './comps/king'
import { Match } from './comps/match'
import {MessageType, createAction} from '@christian/api-interfaces'

const connections = new Set()

export const wsHandler = (ws) => {
    const handleMessage = ( message: string ) => {
        const parsed = JSON.parse(message)
        console.log('job', parsed)
        // @ts-ignore
        switch(parsed.type){
            case MessageType.Get:
                broadcastKings()
                broadcastMatch()
            break;
            //#region  Kings
            case MessageType.StartTimer: 
                kings.startTimer()
            break;
            case MessageType.StopTimer: 
                kings.stopTimer()
            break;
            case MessageType.SetTimer:
                kings.setTimer(parsed.payload)
            break;
            case MessageType.KingWin: 
                kings.kingWin()
            break
            case MessageType.ContenderWin:
                kings.contenderWin()
            break;
            case MessageType.MissedServe:
                kings.kingWin(parsed.payload)
            break;
            case MessageType.AddPoint: 
                kings.addPoint(parsed.payload)
            break;
            case MessageType.RemovePoint:
                kings.removePoint(parsed.payload)
            break;
            case MessageType.MakeKing:
                kings.makeKing(parsed.payload)
            break;
            case MessageType.MakeContender:
                kings.makeContender(parsed.payload)
            break;
            case MessageType.AddTeam:
                kings.addTeam(parsed.payload)
            break;
            case MessageType.RemoveTeam:
                kings.removeTeam(parsed.payload)
            break;
            case MessageType.MoveUp:
                kings.moveUp(parsed.payload)
            break;
            case MessageType.MoveDown:
                kings.moveDown(parsed.payload)
            break;
            case MessageType.ResetKings:
                kings.reset()
            break;
            //#endregion 
            
            //#region Match
            case MessageType.MatchAddPoint:
                match.addPoint(parsed.payload)
            break
            
            case MessageType.MatchRemovePoint:
                match.removePoint(parsed.payload)
            break;
            
            case MessageType.MatchApplyTeams:
                match.applyTeams(parsed.payload)
            break;

            case MessageType.MatchReset:
                match.resetMatch()
            break;

            case MessageType.MatchEndSet:
                match.endSet()
            break;

            case MessageType.MatchSwitchSides:
                match.matchSwitchSides()
            break;
            //#endregion
        }
    }

    const createKingsAction = () => { 
        return createAction(MessageType.Kings, kings.getKings())
    }

    const createMatchAction =()=>{
        return createAction(MessageType.Match, match.getMatch())
    }

    const broadcastKings = () => {
        //TODO: check ready state
        // @ts-ignore
        connections.forEach(conn => {
            // @ts-ignore
            if (conn.readyState === 1){
                // @ts-ignore
                conn.send(createKingsAction())    
            }
        })
    }

    const broadcastMatch = () => {
        //TODO: check ready state
        // @ts-ignore
        connections.forEach(conn => {
            // @ts-ignore
            if (conn.readyState === 1){
                // @ts-ignore
                conn.send(createMatchAction())    
            }
        })
    }

    // Add the connection to our set
    connections.add(ws)

    ws.send(createKingsAction())
    ws.send(createMatchAction())

    ws.on('message', (message) => {
        handleMessage(message)
    })

    ws.on('error', (err)=>{
        console.log('error', err)
    })

    ws.on('close', () => {
        connections.delete(ws)
    })

    // set the broadcast method for kings to use. 
    kings.setBroadcast(broadcastKings)
    match.setBroadcast(broadcastMatch)
}


const kings = new Kings()

const match = new Match()






