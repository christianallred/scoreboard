
import { ICompetitor, Color } from '@christian/api-interfaces';
import { cloneDeep } from 'lodash'

interface IKings {
    teams: ICompetitor[]
    timer: number
}

// TODO: add a way to periodically save it to disk, we cn astore in a json fiel for now. 
export class Kings {
    
    teams: ICompetitor[] = testNextUp
    
    timer: number = 0

    timerReference
    
    broadcast

    constructor () {
    }
    
    setBroadcast(broadcast){
        this.broadcast = broadcast
    }

    getKings(): IKings {        
        return {
            teams: this.teams,
            timer: this.timer
        }
    }

    startTimer() {
        this.tick()
    }

    stopTimer() {
        clearTimeout(this.timerReference)
    }

    setTimer(minutes: number){
        this.stopTimer()
        this.timer = minutes * 60
        this.broadcast()
    }
    
    private tick() {
        if (this.timer === 0) {
            this.stopTimer()
        } else {
            this.timerReference = setTimeout(() => {
                this.timer = this.timer - 1
                this.broadcast()
                this.tick()
            }, 1000)
        }
    }

    kingWin(missedServe?: boolean){
        const king = cloneDeep(this.teams.find(team => team.kingside))
        if (!missedServe){
            king.score = king.score + 1
            king.streak = king.streak + 1
        }
        
        const newContender = cloneDeep(this.teams.find(team => team.order === 1))
        newContender.order = null
        newContender.contender = true

        const newNextUp = cloneDeep(this.teams.filter(team => team.order > 1))
        newNextUp.forEach(team => {
            team.order = team.order - 1
        })


        const contender = this.teams.find(team => team.contender)
        contender.contender = false
        contender.order = newNextUp.length + 1

        this.teams = [
            king, 
            newContender,
            ...newNextUp,
            cloneDeep(contender)
        ]
        this.broadcast()
    }
    
    contenderWin(){
        const newKing = cloneDeep(this.teams.find(team => team.contender))
        newKing.kingside = true
        newKing.contender = false
        newKing.streak = 0

        const newContender = cloneDeep(this.teams.find(team => team.order === 1))
        newContender.order = null
        newContender.contender = true

        const newNextUp = cloneDeep(this.teams.filter(team => team.order > 1))
        newNextUp.forEach(team => {
            team.order = team.order - 1
        })

        const king = cloneDeep(this.teams.find(team => team.kingside))
        king.kingside = false
        king.order = newNextUp.length + 1
        if (king.streak > king.longestStreak){
            king.longestStreak = king.streak + 0
        }
        king.streak = 0

        this.teams = [
            newKing,
            newContender,
            ...newNextUp,
            king,
        ]

        this.broadcast()
    }

    addPoint(guid: string){
        this.teams.find(team => team.guid === guid).score++
        this.broadcast()
    }

    removePoint(guid: string){
        this.teams.find(team => team.guid === guid).score-- 
        this.broadcast()
    }

    makeKing(guid: string){
        const current = this.teams.find(t => t.guid === guid)
        if (current.kingside){
            return
        }

        this.teams.forEach(team => {
            if(team.guid === guid){
                team.kingside = true
                team.contender = false
                team.order = null
            } else {
                if (team.kingside){
                    if(team.streak > team.longestStreak){
                        team.longestStreak = team.streak
                    }
                }
                team.kingside = false
            }
        })

        const nextList = this.teams.filter(team => !team.kingside && !team.contender && team.order)
        const orphan = this.teams.filter(team => !team.kingside && !team.contender && !team.order)
        
        if (orphan.length){
            nextList.push(...orphan)
        }

        nextList.forEach((t,i)=>{
            t.order = i+1
        })

        const newKing = this.teams.filter(t => t.kingside)
        const newContender = this.teams.filter(t => t.contender)

        this.teams = [
            ...newKing,
            ...newContender,
            ...nextList
        ]

        this.broadcast()
    }

    makeContender(guid: string){
         const current = this.teams.find(t => t.guid === guid)
        if (current.contender){
            return
        }

        this.teams.forEach(team => {
            if(team.guid === guid){
                team.kingside = false
                team.contender = true
                team.order = null
            } else {
                team.contender = false
            }
        })

        const nextList = this.teams.filter(team => !team.kingside && !team.contender && team.order)
        const orphan = this.teams.filter(team => !team.kingside && !team.contender && !team.order)
        
        if (orphan.length){
            nextList.push(...orphan)
        }

        nextList.forEach((t,i)=>{
            t.order = i+1
        })

        const newKing = this.teams.filter(t => t.kingside)
        const newContender = this.teams.filter(t => t.contender)

        this.teams = [
            ...newKing,
            ...newContender,
            ...nextList
        ]

        this.broadcast()
    }

    addTeam(payload) {
        this.teams.push(
            this.createCompetitor(payload.name, payload.color)
        )
        this.broadcast()
    }

    removeTeam(guid: string) {   
        this.teams = this.teams.filter(team => team.guid !== guid);
        this.broadcast()
    }

    moveUp(guid:string){
        const moving = this.teams.find(team => team.guid === guid)
        const flip = this.teams.find(team =>team.order === moving.order - 1)
        
        moving.order = moving.order - 1
        flip.order = flip.order + 1
        
        this.broadcast()
    }

    moveDown(guid:string){
        const moving = this.teams.find(team => team.guid === guid)
        const flip = this.teams.find(team =>team.order === moving.order + 1)
        if (!flip){
            return
        }
        moving.order = moving.order + 1
        flip.order = flip.order - 1
        
        this.broadcast()
    }

    reset() {
        this.teams = []
        this.stopTimer()
        this.timer = 0
        this.broadcast()
    }

    private createCompetitor(name: string, color: Color): ICompetitor{
        const makeAsKing = this.teams.length === 0
        const makeAsContender = this.teams.length === 1

        const team = cloneDeep({
            guid: uuidv4(), 
            teamname: name.toUpperCase(),
            color: color,
            score: 0,
            kingside: makeAsKing,
            contender: makeAsContender,
            order: makeAsKing || makeAsContender ? null : this.teams.filter(team => !team.kingside && !team.contender).length + 1,
            streak: 0,
            longestStreak: 0,
        })
        return team;
    } 
}


function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


const testNextUp =  [ 
     {
        guid: uuidv4(),
        teamname: 'Bill & B0b',
        color: Color.Purple,
        score: 1,
        kingside: true,
        contender: false,
        order: null,
        streak: 0,
        longestStreak: 0
    }, 
    {
        guid: uuidv4(),
        teamname: 'SAM & TED',
        color: Color.Pink,
        score: 1,
        kingside: false,
        contender: true,
        order: null,
        streak: 0,
        longestStreak: 0
    },
    {
        guid: uuidv4(),
        teamname: 'HIL & VIG',
        color: Color.Green,
        score: 7,
        kingside: false,
        contender: false,
        order: 1,
        streak: 0,
        longestStreak: 0
    },
    {
        guid: uuidv4(),
        teamname: 'TYE & TYE',
        color: Color.Yellow,
        score: 3,
        kingside: false,
        contender: false,
        order: 2,
        streak: 0,
        longestStreak: 0
    },
    {
        guid: uuidv4(),
        teamname: 'dic & har',
        color: Color.Blue,
        score: 1,
        kingside: false,
        contender: false,
        order: 3,
        streak: 0,
        longestStreak: 0
    }, 
]