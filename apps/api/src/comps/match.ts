import {
    Color, 
    HomeAway, 
    MatchCompetitor,
    IMatch,
    IApplyTeamAction
} from '@christian/api-interfaces'
import {cloneDeep} from 'lodash'


const defaultHome: MatchCompetitor = {
        name: 'Home',
        sets: [0],
        color: Color.Red
    }
const defaultAway: MatchCompetitor = {
        name: 'Away',
        sets: [0],
        color: Color.Blue
    }
    

export class Match {
    set: number = 0

    home: MatchCompetitor = cloneDeep(defaultHome)

    away: MatchCompetitor = cloneDeep(defaultAway)
    
    broadcast

    constructor () {
    }
    
    setBroadcast(broadcast){
        this.broadcast = broadcast
    }

    getMatch():IMatch{
        return {
            home: this.home,
            away: this.away,
            set: this.set,
        }
    }

    addPoint(homeaway: HomeAway){
        this[homeaway].sets[this.set] ++
        this.broadcast()
    }

    removePoint(homeaway: HomeAway){
        this[homeaway].sets[this.set] --
        this.broadcast()
    }

   

    matchSwitchSides(){
        const newHome = cloneDeep(this.away)
        const newAway = cloneDeep(this.home)
        this.home = newHome
        this.away = newAway
        this.broadcast()
    }

    endSet(){
        this.home.sets.push(0)
        this.away.sets.push(0)
        this.set++
        
        this.broadcast()
    }

     resetMatch(){
        this.home = cloneDeep(defaultHome)
        this.away = cloneDeep(defaultAway)
        this.set = 0

        this.broadcast()
    }

    applyTeams(applyTeams: IApplyTeamAction ){
        this.home.name = applyTeams.home.name
        this.home.color = applyTeams.home.color
        this.away.name = applyTeams.away.name
        this.away.color = applyTeams.away.color

        this.broadcast()
    }

    setAwayName(name: string){
        this.away.name = name
        this.broadcast()
    }

}








